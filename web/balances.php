<?php 

$file = 'balances.json';


# PUT
if ($_SERVER['REQUEST_METHOD'] == 'PUT') { try {
        if (isset($_GET['key']) && $_GET['key'] == '2330d702f6a145e8ac6b0b48b77fbbd2') {
            $jsonString = file_get_contents('php://input');
            file_put_contents($file, $jsonString);
            http_response_code(200);
        }
        else {
            http_response_code(403);
        }
    }
    catch (Exception $e) {
        http_response_code(500);
    }
}

else if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    try {
        if (isset($_GET['key']) && $_GET['key'] == '2330d702f6a145e8ac6b0b48b77fbbd2') {
            http_response_code(200);
            header('Content-Type: text/javascript');
            print file_get_contents($file);
        }
        else {
            http_response_code(403);
        }
    }
    catch (Exception $e) {
        http_response_code(500);
    }
}

else {
    http_response_code(405);
}


?>
