import json
from config import Config
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def getdata(debug):
    driver = webdriver.Firefox()
    driver.implicitly_wait(20)
    wait = WebDriverWait(driver, 10)

    data = {'Balances': {}}

    try:
        driver.get('https://wwws.mint.com/login.event')


        print 'Entering username and password'
        driver.find_element_by_id('ius-userid').send_keys(Config.creds['mint']['username'])

        driver.find_element_by_id('ius-password').send_keys(Config.creds['mint']['password'])

        driver.find_element_by_id('ius-sign-in-submit-btn').click()

        # Multi-factor authentication
        if len(driver.find_elements_by_id('ius-mfa-wrapper')) > 0:
            # Select 'Confirm my account another way'
            wait.until(EC.presence_of_element_located((By.ID, 'ius-mfa-wrapper')))
            print 'Entering multi-factor auth data'
            driver.find_element_by_id('ius-mfa-option-confirm-another-way').click();
            driver.find_element_by_id('ius-mfa-options-submit-btn').click();
            driver.find_element_by_id('ius-first-name').send_keys(Config.creds['mint']['firstname'])
            driver.find_element_by_id('ius-last-name').send_keys(Config.creds['mint']['lastname'])
            driver.find_element_by_id('ius-dob').send_keys(Config.creds['mint']['dob'])
            driver.find_element_by_id('ius-ssn').send_keys(Config.creds['mint']['ssn'])
            driver.find_element_by_id('ius-city').send_keys(Config.creds['mint']['city'])
            driver.find_element_by_id('ius-state').send_keys(Config.creds['mint']['state'])
            driver.find_element_by_id('ius-zip').send_keys(Config.creds['mint']['zip'])
            driver.find_element_by_id('ius-street-address').send_keys(Config.creds['mint']['addr'])
            #driver.find_element_by_id('ius-idp-pii-submit-btn').click()
            wait.until(EC.element_to_be_clickable((By.ID, 'ius-idp-pii-submit-btn'))).click()
        
        else:
            print 'No mfa required'

#        element = wait.until(EC.presence_of_element_located((By.ID, 'asrEmail')))
#        cashRows = driver.find_elements_by_css_selector('#cash tbody tr')
#        for row in cashRows:
#            accountName = row.find_element_by_css_selector('a.account').text
#            accountAmount = row.find_element_by_css_selector('a.amount').text
#            data['Balances'][accountName] = accountAmount
#
#        print json.dumps(data)
    finally:
        if debug:
            return driver
        else:
            driver.quit()

if __name__ == "__main__":
    getdata(False)
