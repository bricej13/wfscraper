import os, json
from datetime import datetime
from config import Config
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def login():
    os.environ["DISPLAY"] = ":99"
    driver = webdriver.Firefox()
    driver.implicitly_wait(20)
    wait = WebDriverWait(driver, 10)

    try:
        driver.get('https://wellsfargo.com')

        usernameField = driver.find_element_by_id('userid')
        usernameField.send_keys(Config.creds['wf']['username'])

        passwordField = driver.find_element_by_id('password')
        passwordField.send_keys(Config.creds['wf']['password'])

        signon = driver.find_element_by_id('btnSignon')
        signon.click()

        element = wait.until(EC.presence_of_element_located((By.ID, 'asrEmail')))
	driver.save_screenshot('afterlogin.png')
    finally:
        return driver

def getwfdata():
    data = {'balances': {}, 'transactions': {}}

    try:
        driver = login()
        wait = WebDriverWait(driver, 10)

        # Get current account balances
        cashRows = driver.find_elements_by_css_selector('#cash tbody tr')
        for row in cashRows:
            accountName = row.find_element_by_css_selector('a.account').text.split()[-1]
            accountAmount = row.find_element_by_css_selector('a.amount').text
            data['balances'][accountName] = accountAmount

         # Get recent transactions
	driver.save_screenshot('afteraccountscrape.png')
        driver.find_element_by_css_selector('#menu2Wrap a:nth-child(2)').click()
        print('Waiting for transactions page to load')
        element = wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, '#DDATransactionTable tbody tr')))
	driver.save_screenshot('transactionpageloaded.png')
        print('Transaction page loaded, finding recent transactions')
        tRows = driver.find_elements_by_css_selector('#DDATransactionTable tbody tr')
        driver.implicitly_wait(0)
        for i, row in enumerate(tRows):
            if i > 10:
                break
            if len(row.find_elements_by_css_selector('td')) > 0:
                cells = row.find_elements_by_css_selector('td,th')
                transaction = {}
                transaction['date'] = cells[0].text.strip()
                transaction['desc'] = cells[1].text.strip()
                transaction['deposit']= cells[2].text.strip()
                transaction['withdrawl']= cells[3].text.strip()

                transaction['amount'] = transaction['deposit'] \
                    if transaction['deposit'] \
                    else transaction['withdrawl'].replace("$", "$-") 

                if len(cells[1].find_elements_by_class_name('showDetailsLink')) > 0:
                    transaction['pending'] = False
                else:
                    transaction['pending'] = True
                
                data['transactions'][str(i)] = transaction
         

	data['timestamp'] = datetime.isoformat(datetime.now())
	data['updated'] = datetime.now().strftime('%b %d %I:%M %p')

	return data
    finally:
        driver.quit()

if __name__ == "__main__":
    data = getwfdata()
    if hasattr(Config, 'output'):
        try:
            outputfile = open(Config.output, 'w')
            outputfile.writelines(data)
            outputfile.close()
        
        except IOException:
            print 'Could not open ' + Config.output + ' file for writing'
        pass
    else:
        print(json.dumps(data))
