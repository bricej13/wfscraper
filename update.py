#!/usr/bin/python
import json, os, requests, wellsfargo 
from config import Config


def putbalancedata(payload):
	r = requests.put(Config.uploadurl, data=json.dumps(payload))


if __name__ == "__main__":
    data = wellsfargo.getwfdata()
    putbalancedata(data)


