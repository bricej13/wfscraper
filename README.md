# What is this?
A terribly documented selenium script to pull wells fargo account information and save it to a JSON file.

# How to use
1. Setup selenium w/ Firefox driver (see Alpine Linux setup section)
2. Clone repository
3. Copy `config.py.sample` to `config.py` & update `config.py`
4. Copy `balances.php` to a web server
5. Update `update.py` to point at your webserver hosting balances.php
6. Add `update.py` to crontab

## Alpine Linux Selenium Setup
Enable all 'edge' repositories in `/etc/apk/repositories`

```bash
apk update
apk add xvfb firefox dbus py-pip ttf-dejavu

dbus-uuidgen > /etc/machine-id
Xvfb :99 -ac &
export DISPLAY=:99

pip install --upgrade pip
pip install selenium requests
git clone https://bricej13@bitbucket.org/bricej13/wfscraper.git
```
### Start on Xvfb Boot
Enable local scripts: `rc-update add local`

Create the following file in `/etc/local.d/Xvfb.start`
```bash
#!/bin/sh
/usr/bin/Xvfb :99 -ac &
```
`chmod +x /etc/local.d/Xvfb.start`